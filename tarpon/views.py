from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.views import generic
from django.views.generic import View
from .models import Album
from .forms import UserForm

class IndexView(generic.ListView):
	template_name = 'tarpon/index.html'
	context_object_name = 'all_albums'

	def get_queryset(self):
		return Album.objects.all()

class DetailView(generic.DetailView):
	model = Album
	template_name = 'tarpon/details.html'

class AlbumCreate(CreateView):
	model = Album
	fields = ['artist', 'album_title', 'genre', 'album_cover']

class AlbumUpdate(UpdateView):
	model = Album
	fields = ['artist', 'album_title', 'genre', 'album_cover']

class AlbumDelete(DeleteView):
	model = Album
	success_url = reverse_lazy('tarpon:index')

class UserFormView(View):
	form_class = UserForm
	template_name = 'tarpon/registration_form.html'

	# display blank form
	def get(self, request):
		form = self.form_class(None)
		return render(request, self.template_name, {'form': form})

	# process user registration data
	def post(self, request):
		form = self.form_class(request.POST)

		if form.is_valid():

			user = form.save(commit=False)

			# cleaned or normalized data
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user.set_password(password)
			user.save()

			# returns User objects if credentials are correct
			user = authenticate(username=username, password=password)

			if user is not None:

				if user.is_active:
					login(request, user)
					return redirect('tarpon:index')

		return render(request, self.template_name, {'form': form})

""" Old Now-unused code
from django.http import Http404, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template import loader
from .models import Album

def index(request):
	all_albums = Album.objects.all()
	
	context = { 'all_albums' : all_albums }
	return render(request, 'tarpon/index.html', context)

def details(request, album_id):
	#try:
	#	album = Album.objects.get(id=album_id)
	#except Album.DoesNotExist:
	#	raise Http404("Album is Dead")
	album = get_object_or_404(Album, id=album_id)

	context = { 'album' : album }
	return render(request, 'tarpon/details.html', context)

def favorite(request, album_id):
	album = get_object_or_404(Album, id=album_id)
	try:
		selected_song = album.song_set.get(id=request.POST['song'])
	except (KeyError, Song.DoesNotExist):
		context = {
			'album' : album,
			'error_message' : "You did not select a valid song",
		}
		return render(request, 'tarpon/details.html', context)
	else:
		selected_song.is_favorite = True
		selected_song.save()

	context = { 'album': album, }
	return render(request, 'tarpon/details.html', context)
"""