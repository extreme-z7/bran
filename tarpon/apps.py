from django.apps import AppConfig


class TarponConfig(AppConfig):
    name = 'tarpon'
