from django.apps import AppConfig


class WedgeConfig(AppConfig):
    name = 'wedge'
